N = 1000
print(sum(num for num in range(N) if num % 3 == 0 or num % 5 == 0))